# F

## Content

```
./F. Astor:
F. Astor - Regele argintului din Nevada 1.0 '{Western}.docx

./F. W. Bailes:
F. W. Bailes - Puterea tamaduitoare a mintii 0.99 '{Spiritualitate}.docx

./Fabio Fernandes:
Fabio Fernandes - Cum sa incepi o poveste 0.99 '{SF}.docx
Fabio Fernandes - Vampirul si geneticianul 0.9 '{Vampiri}.docx

./Fadia Faqir:
Fadia Faqir - Ma numesc Salma 1.0 '{Literatura}.docx
Fadia Faqir - Stalpi de sare 1.0 '{Literatura}.docx

./Fanny Liviu Rebreanu:
Fanny Liviu Rebreanu - Cu sotul meu 0.7 '{ClasicRo}.docx

./Fanus Neagu:
Fanus Neagu - Covorul violet 0.9 '{ClasicRo}.docx
Fanus Neagu - Dincolo de nisipuri 1.0 '{ClasicRo}.docx
Fanus Neagu - Frumosii nebuni ai marilor orase 0.6 '{ClasicRo}.docx
Fanus Neagu - Ingerul a strigat 2.0 '{ClasicRo}.docx
Fanus Neagu - Scaunul singuratatii 1.0 '{ClasicRo}.docx
Fanus Neagu - Zeul ploii 0.9 '{ClasicRo}.docx

./Farah Pahlavi:
Farah Pahlavi - Memorii 0.9 '{Memorii}.docx

./Farida Khalaf:
Farida Khalaf - Povestea Faridei. Fata care a invins Isis 1.0 '{Literatura}.docx

./Farley Mowat:
Farley Mowat - In nordul indepartat 1.0 '{AventuraTineret}.docx
Farley Mowat - Sa nu ne temem de lupi 1.0 '{AventuraTineret}.docx

./Faye Kellerman:
Faye Kellerman - Pe marginea intunericului 1.0 '{Razboi}.docx

./Faye Morgan:
Faye Morgan - Avocatul public 0.99 '{Romance}.docx
Faye Morgan - Decizie periculoasa 0.9 '{Dragoste}.docx

./Fayrene Preston:
Fayrene Preston - A doua luna de miere 0.9 '{Dragoste}.docx
Fayrene Preston - Flacari de argint 0.9 '{Dragoste}.docx
Fayrene Preston - Un inger fermecator 1.0 '{Romance}.docx

./Fazil Iskander:
Fazil Iskander - Sa-i dam de rusine pe falsificatorii de bani! 1.0 '{Povestiri}.docx
Fazil Iskander - Zodia kozloturului 1.0 '{Literatura}.docx

./Federico Axat:
Federico Axat - Ultima scapare 1.0 '{Thriller}.docx

./Felicia Sanda Bugnar:
Felicia Sanda Bugnar - Omul de zapada 0.99 '{Teatru}.docx

./Felix Aderca:
Felix Aderca - Orasele scufundate 2.0 '{SF}.docx

./Felix Celval:
Felix Celval - Captiva in insula vampirilor 1.0 '{Tineret}.docx
Felix Celval - In iadul verde 1.0 '{Tineret}.docx

./Felix de Aziia:
Felix de Aziia - Povestea unui idiot spusa de el insusi 0.7 '{Diverse}.docx

./Felix Moga:
Felix Moga - Cronotron 0.99 '{SF}.docx

./Felix Topescu:
Felix Topescu - Calaria pentru toti 0.9 '{Diverse}.docx

./Felix Tzele:
Felix Tzele - Nu am mai privit in urma 2.0 '{SF}.docx

./Fenimore Cooper:
Fenimore Cooper - Leii de mare 1.0 '{Western}.docx

./Feodor Abramov:
Feodor Abramov - Casa 1.0 '{Literatura}.docx

./Feodor Mihailovici Dostoievski:
Feodor Mihailovici Dostoievski - Adolescentul 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Amintiri din casa mortilor 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Crima si pedeapsa 2.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Crocodilul - O intamplare neobisnuita 0.6 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Demonii 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Domnul Proharcin 0.9 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Dublul 0.6 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Eternul sot 0.99 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Fratii Karamazov 1.1 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Fratii Karamazov V1 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Fratii Karamazov V2 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Gazda 0.7 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Idiotul 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Inima slaba 0.9 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Insemnari din subterana 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Jucatorul 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Jurnal de scriitor 0.99 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Micul erou 0.8 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Netoska Nezvanova 0.7 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Nevasta altuia si un sot sub pat 0.7 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Nopti albe 0.99 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Opere V1 2.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Opere V2 2.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Opere V3 2.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Opere V4 1.0 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Polzunkov 0.8 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Stepancikovo si locuitorii sai 0.7 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Un pom de Craciun si o nunta 0.6 '{ClasicSt}.docx
Feodor Mihailovici Dostoievski - Un roman in noua scrisori 0.7 '{ClasicSt}.docx

./Feodor Sologub:
Feodor Sologub - Demonul meschin 0.9 '{Diverse}.docx

./Fernand Fournier Aubry:
Fernand Fournier Aubry - Don Fernando 1.1 '{Aventura}.docx

./Fernando Alegria:
Fernando Alegria - Lautaro 2.0 '{Western}.docx

./Fernando Vallejo:
Fernando Vallejo - Sfanta feciora a ucigasilor platiti 1.0 '{Literatura}.docx
Fernando Vallejo - Si vom merge toti in infern 1.0 '{Literatura}.docx

./Fern Michaels & Cathy Lamb:
Fern Michaels & Cathy Lamb - O dorinta de Craciun 1.0 '{Aventura}.docx

./Ferran Torrent:
Ferran Torrent - Bulevardul francezilor 0.99 '{Literatura}.docx

./Ferreira de Castro:
Ferreira de Castro - Emigrantii 1.0 '{Diverse}.docx

./Filip Florian:
Filip Florian - Degete mici 0.8 '{Diverse}.docx

./Filip Jansky:
Filip Jansky - Cavalerii cerului 1.0 '{Razboi}.docx

./Filip Petru:
Filip Petru - Clepsidra 0.9 '{ProzaScurta}.docx

./Fiona Barton:
Fiona Barton - Copilul 1.0 '{Literatura}.docx
Fiona Barton - Vaduva 1.0 '{Thriller}.docx

./Fiona Moore:
Fiona Moore - Castelul dintre stejari 0.9 '{Dragoste}.docx

./Flavia Buref:
Flavia Buref - Intamplari surazatoare 1.0 '{Umor}.docx

./Flavius Florentin Kiss:
Flavius Florentin Kiss - Ucigas de sentimente 2.0 '{ProzaScurta}.docx

./Flavius Guias:
Flavius Guias - A fi sau a nu fi modern 0.9 '{Spiritualitate}.docx

./Flavius Josephus:
Flavius Josephus - Antichitati iudaice, cartea I-XX 1.0 '{Religie}.docx

./Flavius Policalas:
Flavius Policalas - Secretul lui Abbott 1.9 '{SF}.docx

./Floarea Nicola:
Floarea Nicola - Sadoveanu vocatia sacrului 0.9 '{Diverse}.docx

./Flora Kidd:
Flora Kidd - De-a casatoria 0.8 '{Romance}.docx
Flora Kidd - Jocul cu focul 0.99 '{Romance}.docx
Flora Kidd - Sebastian si Aurore 1.0 '{Romance}.docx

./Florence Barclay:
Florence Barclay - Viata mea iti apartine 0.9 '{Diverse}.docx

./Florence Scovell Shin:
Florence Scovell Shin - V1 Jocul vietii 0.9 '{Spiritualitate}.docx
Florence Scovell Shin - V2 Cuvantul vostru este o bagheta magica 0.9 '{Spiritualitate}.docx

./Florentin Haidamac:
Florentin Haidamac - Adnana 1.0 '{SF}.docx

./Florian David:
Florian David - Si-a sapat singur groapa 5.0 '{SF}.docx

./Florian Garz:
Florian Garz - CIA contra KGB 0.7 '{Istorie}.docx
Florian Garz - Spionajul total in actiune 0.6 '{Istorie}.docx

./Florian Grecea:
Florian Grecea - Profesorul de dans 0.7 '{Politista}.docx

./Florian Oprea:
Florian Oprea - Marea intoarcere 0.9 '{Politista}.docx
Florian Oprea - Tainele unui dosar 1.0 '{Politista}.docx

./Florian Zeller:
Florian Zeller - Fascinatia raului 0.7 '{Diverse}.docx

./Florica T. Cimpan:
Florica T. Cimpan - Povestea numerelor 0.9 '{Diverse}.docx

./Florina Ilis:
Florina Ilis - Cinci nori colorati pe cerul de rasarit 0.7 '{Diverse}.docx

./Florina Jinga:
Florina Jinga - Draga Dumnezeu 0.9 '{Diverse}.docx

./Florin Andrei Ionescu:
Florin Andrei Ionescu - Spionul ma privi surprins 1.0 '{Politista}.docx
Florin Andrei Ionescu - Unchiul dumneavoastra e brunet 1.0 '{Politista}.docx

./Florin Banescu:
Florin Banescu - Portocale pentru vinovati 1.0 '{Politista}.docx
Florin Banescu - Semintele diminetii 1.0 '{CalatorieinTimp}.docx

./Florin Cojocariu:
Florin Cojocariu - Povestea cavalerului de iarba verde 0.9 '{Diverse}.docx

./Florin Gheorghita:
Florin Gheorghita - Incursiuni in alte lumi 1.0 '{MistersiStiinta}.docx
Florin Gheorghita - Magia realitatilor paralele 1.0 '{Spiritualitate}.docx
Florin Gheorghita - OZN eterice 0.7 '{MistersiStiinta}.docx
Florin Gheorghita - Revenirea zeilor 1.0 '{MistersiStiinta}.docx
Florin Gheorghita - Veghetorii Terrei 1.0 '{MistersiStiinta}.docx

./Florin Horvath:
Florin Horvath - O lacrima pentru Maria sa 1.0 '{IstoricaRo}.docx

./Florin Lazarescu:
Florin Lazarescu - Pulp fiction 0.99 '{Necenzurat}.docx
Florin Lazarescu - Trimisul nostru special 0.8 '{Diverse}.docx

./Florin Loghin:
Florin Loghin - La intersectia caravanelor vietii 1.0 '{Diverse}.docx

./Florin Manolescu:
Florin Manolescu - Misterul camerei inchise 0.8 '{Diverse}.docx

./Florin Mugur:
Florin Mugur - Convorbiri cu Marin Preda 1.0 '{ClasicRo}.docx

./Florin Oprea & Livia Ardelean:
Florin Oprea & Livia Ardelean - Rebus la Mamaia 1.0 '{ClubulTemerarilor}.docx

./Florin Patea:
Florin Patea - Anorganic 0.7 '{SF}.docx
Florin Patea - Fictiuni 20-00 0.9 '{SF}.docx
Florin Patea - Necropolis 0.9 '{SF}.docx
Florin Patea - Soiuri de peste 0.99 '{SF}.docx
Florin Patea - Veniti afara 0.99 '{SF}.docx

./Florin Petrescu:
Florin Petrescu - Lacul suspendat 1.0 '{SF}.docx

./Florin Ursu:
Florin Ursu - Olar cu puncte 5.0 '{SF}.docx

./Florin Vasiliu:
Florin Vasiliu - De la Pearl Harbor la Hiroshima 1.0 '{Razboi}.docx
Florin Vasiliu - Pe meridianul Yamatto 0.7 '{Calatorii}.docx

./Flynn Berry:
Flynn Berry - Sora pierduta 1.0 '{Thriller}.docx

./Forrest Leo:
Forrest Leo - Un gentleman 1.0 '{Literatura}.docx

./Frances Davies:
Frances Davies - O americana in Mexic 0.9 '{Dragoste}.docx

./Frances Flores:
Frances Flores - Secrete de nemarturisit 1.0 '{Romance}.docx
Frances Flores - Vinul iubirii 1.0 '{Romance}.docx

./Frances Lloyd:
Frances Lloyd - Fara menajamente 0.99 '{Dragoste}.docx
Frances Lloyd - Mireasa regasita 0.9 '{Dragoste}.docx

./Francine Pascal:
Francine Pascal - Eroare aproape fatala 0.99 '{Romance}.docx
Francine Pascal - Seducatoarea intriganta 0.9 '{Romance}.docx

./Francine Rivers:
Francine Rivers - Ultimul devorator de pacate 1.0 '{Literatura}.docx

./Francis Carsac:
Francis Carsac - Robinsonii Cosmosului 1.0 '{SF}.docx

./Francisca Solar:
Francisca Solar - Al 7-lea M 1.0 '{Politista}.docx

./Francis Clifford:
Francis Clifford - Lupta sa invingi 1.0 '{ActiuneComando}.docx

./Francisc Munteanu:
Francisc Munteanu - Barajul 1.0 '{Literatura}.docx
Francisc Munteanu - Cocorii zboara fara busola 1.0 '{Literatura}.docx
Francisc Munteanu - Cont secret 1.0 '{Literatura}.docx
Francisc Munteanu - Dincolo de ziduri 1.0 '{Literatura}.docx
Francisc Munteanu - Hotel Tristete si alte povestiri 1.0 '{Literatura}.docx
Francisc Munteanu - Incotro 1.0 '{Literatura}.docx
Francisc Munteanu - In orasul de pe Mures 1.0 '{Literatura}.docx
Francisc Munteanu - Lenta 1.0 '{Literatura}.docx
Francisc Munteanu - Pistruiatul 1.0 '{AventuraTineret}.docx
Francisc Munteanu - Printesa din Sega 1.0 '{Literatura}.docx
Francisc Munteanu - Reintoarcerea 1.0 '{AventuraTineret}.docx
Francisc Munteanu - Scrisoarea 1.0 '{Literatura}.docx
Francisc Munteanu - Scrisori din Calea Lactee 1.0 '{Literatura}.docx
Francisc Munteanu - Sonata in Re Major 1.0 '{Literatura}.docx
Francisc Munteanu - Statuile nu rad niciodata 1.0 '{Literatura}.docx
Francisc Munteanu - Terra di Siena 0.9 '{Literatura}.docx

./Francisco Garcia Pavon:
Francisco Garcia Pavon - Domnia lui Witiza 1.0 '{Politista}.docx

./Francisc Pal:
Francisc Pal - Schite 0.8 '{ProzaScurta}.docx

./Franciscus Georgius:
Franciscus Georgius - Lampa de veghe 1.0 '{Diverse}.docx

./Francis de Croisset:
Francis de Croisset - Doamna din Malacca 1.0 '{Dragoste}.docx

./Francis Dessart:
Francis Dessart - Onoare nationala si solidaritate umana. itinerariul spiritual al lui Gandhi 0.8 '{Diverse}.docx

./Francis Parkman:
Francis Parkman - Pe calea Oregonului 1.0 '{Western}.docx

./Francis Schaeffer:
Francis Schaeffer - Dans de unul singur 0.2 '{Religie}.docx
Francis Schaeffer - Trilogia 0.8 '{Religie}.docx

./Francis Scott Fitzgerald:
Francis Scott Fitzgerald - Blandetea noptii 1.0 '{Dragoste}.docx
Francis Scott Fitzgerald - Cei frumosi si blestemati 2.0 '{Dragoste}.docx
Francis Scott Fitzgerald - Marele Gatsby 1.0 '{Dragoste}.docx
Francis Scott Fitzgerald - Povestiri cu Pat Hobby 0.9 '{Dragoste}.docx
Francis Scott Fitzgerald - Un diamant cat hotelul Ritz 0.99 '{Dragoste}.docx

./Francois Begaudeau:
Francois Begaudeau - In clasa 0.8 '{Diverse}.docx

./Francois Cheng:
Francois Cheng - Vesnicia ne asteapta 1.0 '{Dragoste}.docx

./Francois Clement:
Francois Clement - Asa s-a nascut o insula 1.0 '{SF}.docx

./Francoise Augier:
Francoise Augier - Iubire de departe 1.0 '{Romance}.docx

./Francoise Dolto:
Francoise Dolto - Cand apare copilul 1.0 '{Copii}.docx

./Francoise Dorin:
Francoise Dorin - In numele tatalui si al fiicei 1.0 '{Literatura}.docx

./Francoise Sagan:
Francoise Sagan - Va place Brahms 1.0 '{Dragoste}.docx

./Francois Mauriac:
Francois Mauriac - Cuibul de vipere 0.7 '{ClasicSt}.docx
Francois Mauriac - Genitrix. Misterul Frontenac 1.0 '{ClasicSt}.docx
Francois Mauriac - Ingerii negri 1.0 '{ClasicSt}.docx
Francois Mauriac - Sfarsitul noptii. Sarutul dat leprosului 1.0 '{ClasicSt}.docx
Francois Mauriac - Therese Desqueyroux 0.8 '{ClasicSt}.docx
Francois Mauriac - Un adolescent de altadata. Maltaverne 1.0 '{ClasicSt}.docx

./Francois Rabelais:
Francois Rabelais - Gargantua si Pantagruel 0.9 '{ClasicSt}.docx

./Francois Villon:
Francois Villon - Balade 0.9 '{Versuri}.docx
Francois Villon - Din volumul poezii (1968) 0.8 '{Versuri}.docx
Francois Villon - Poezii 0.9 '{Versuri}.docx
Francois Villon - Urcus 0.99 '{Versuri}.docx

./Frank Arnau:
Frank Arnau - Pielea ingerului negru 1.0 '{Politista}.docx
Frank Arnau - Societatea anonima Heroina 1.0 '{Politista}.docx

./Frank E. Peretti:
Frank E. Peretti - Blestemul de moarte din Tocorey 0.9 '{AventuraTineret}.docx

./Frank Gordon:
Frank Gordon - Spionaj in purpura 1.0 '{Politista}.docx

./Frank Herbert:
Frank Herbert - Ciuma alba 4.1 '{SF}.docx
Frank Herbert - Dune - V0 Anexe 1.0 '{SF}.docx
Frank Herbert - Dune - V1 Dune V1 1.0 '{SF}.docx
Frank Herbert - Dune - V1 Dune V2 0.9 '{SF}.docx
Frank Herbert - Dune - V2 Mantuitorul Dunei 1.0 '{SF}.docx
Frank Herbert - Dune - V3 Copiii Dunei 1.0 '{SF}.docx
Frank Herbert - Dune - V4 Imparatul zeu al Dunei 5.0 '{SF}.docx
Frank Herbert - Dune - V5 Ereticii Dunei 5.0 '{SF}.docx
Frank Herbert - Dune - V6 Canonicatul Dunei 5.0 '{SF}.docx
Frank Herbert - Dune - V7 Cartea Brundurilor 1.0 '{SF}.docx
Frank Herbert - Experimentul Dosadi 1.1 '{SF}.docx
Frank Herbert - Pandora 1 - Incidentul Iisus 5.0 '{SF}.docx
Frank Herbert - Pandora 2 - Efectul Lazar 5.0 '{SF}.docx
Frank Herbert - Pandora 3 - Factorul inaltare 5.0 '{SF}.docx
Frank Herbert - Steaua si biciul 5.1 '{SF}.docx

./Frank Laubach:
Frank Laubach - Traind in prezenta sa 0.99 '{Religie}.docx

./Frank Lewis:
Frank Lewis - Legionarul 1.0 '{ActiuneRazboi}.docx
Frank Lewis - Persanul 1.0 '{ActiuneRazboi}.docx
Frank Lewis - Piratul 1.0 '{ActiuneRazboi}.docx
Frank Lewis - Proiectul Phoenix 1.0 '{ActiuneRazboi}.docx

./Frank M. Robinson:
Frank M. Robinson - O viata in ziua de... 1.0 '{SF}.docx

./Frank Schatzing:
Frank Schatzing - Adancuri 1.0 '{Aventura}.docx

./Frans Bengtsson:
Frans Bengtsson - Vikingii 1.0 '{AventuraIstorica}.docx

./Frans de Waal:
Frans de Waal - Bonobo si ateul. In cautarea umanismului intre primate 2.0 '{Psihologie}.docx

./Frantisek Behounek:
Frantisek Behounek - Actiunea L 2.0 '{SF}.docx

./Franz Bardon:
Franz Bardon - Frabato magicianul 0.99 '{Spiritualitate}.docx
Franz Bardon - Initiere in Hermetism 0.99 '{Spiritualitate}.docx

./Franz Hartmann:
Franz Hartmann - Paracelsus viata si invatatura 1.0 '{Spiritualitate}.docx

./Franz Hoffman:
Franz Hoffman - Inzapeziti 0.9 '{Literatura}.docx

./Franz Kafka:
Franz Kafka - America 0.7 '{Thriller}.docx
Franz Kafka - Colonia penitenciara 1.0 '{Thriller}.docx
Franz Kafka - In fata legii 1.0 '{Thriller}.docx
Franz Kafka - La galerie 1.0 '{Thriller}.docx
Franz Kafka - Metamorfoza 2.0 '{Thriller}.docx
Franz Kafka - O dare de seama pentru Academie 1.0 '{Thriller}.docx
Franz Kafka - Povestiri 0.9 '{Thriller}.docx
Franz Kafka - Procesul 1.1 '{Thriller}.docx
Franz Kafka - Un artist al foamei 1.0 '{Thriller}.docx
Franz Kafka - Un medic de tara 1.0 '{Thriller}.docx
Franz Kafka - Unsprezece feciori 1.0 '{Thriller}.docx
Franz Kafka - Verdictul 1.0 '{Thriller}.docx

./Fratii Grimm:
Fratii Grimm - Povesti 1.0 '{BasmesiPovesti}.docx

./Fred Connelly:
Fred Connelly - Ambuscada in Cuba 1.0 '{ActiuneRazboi}.docx
Fred Connelly - Ciclonul Barrabas 2.0 '{ActiuneRazboi}.docx
Fred Connelly - Escadroanele mortii 1.0 '{ActiuneRazboi}.docx
Fred Connelly - Infern in Honduras 1.0 '{ActiuneRazboi}.docx
Fred Connelly - Unora le place iadul 1.0 '{ActiuneRazboi}.docx

./Frederic Beigbeder:
Frederic Beigbeder - 14.99 euro 0.9 '{Diverse}.docx
Frederic Beigbeder - Dragostea dureaza trei ani 0.99 '{Dragoste}.docx

./Frederic Cabesos:
Frederic Cabesos - Promisiunea ingerului 0.99 '{Literatura}.docx

./Frederick Bailes:
Frederick Bailes - Mintea poate vindeca 4.0 '{Spiritualitate}.docx

./Frederick Forsyth:
Frederick Forsyth - Afganul 2.0 '{ActiuneComando}.docx
Frederick Forsyth - Al patrulea protocol 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Alternativa diavolului 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Cainii razboiului 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Dezinformatorul 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Dosarul Odessa 2.0 '{ActiuneComando}.docx
Frederick Forsyth - Fantoma din Manhattan 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Manifestul negru 2.0 '{ActiuneComando}.docx
Frederick Forsyth - Negociatorul 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Pumnul lui Dumnezeu 2.0 '{ActiuneComando}.docx
Frederick Forsyth - Razbunatorul 1.0 '{ActiuneComando}.docx
Frederick Forsyth - Ziua sacalului 4.0 '{ActiuneComando}.docx

./Frederick Taylor:
Frederick Taylor - Dresda - Marti, 13 februarie 1945 1.0 '{Razboi}.docx

./Frederic Soulie:
Frederic Soulie - Memoriile diavolului 2.0 '{ClasicSt}.docx

./Frederic Valade:
Frederic Valade - Regina nevazutilor 1.0 '{Tineret}.docx

./Frederik Pohl:
Frederik Pohl - Heechee - V1 Poarta 4.0 '{SF}.docx
Frederik Pohl - Heechee - V2 Dincolo de orizontul albastru 2.0 '{SF}.docx
Frederik Pohl - Heechee - V3 Intalnire cu Heechee 2.0 '{SF}.docx
Frederik Pohl - Heechee - V4 Consemnarile Heechee 1.0 '{SF}.docx
Frederik Pohl - Heechee - V5 Drumul prin poarta 1.0 '{SF}.docx
Frederik Pohl - Heechee - V6 Baiatul care vroia sa traiasca vesnic 1.0 '{SF}.docx
Frederik Pohl - Proiectul Omul Plus 2.0 '{SF}.docx
Frederik Pohl - Tunelul de sub lume 1.0 '{SF}.docx

./Frederique Hebrard:
Frederique Hebrard - Septembrie nesigur 0.99 '{Dragoste}.docx

./Fred Hoyle:
Fred Hoyle - La intai octombrie va fi prea tarziu 1.0 '{MistersiStiinta}.docx

./Fred Hoyle & John Elliot:
Fred Hoyle & John Elliot - A de la Andromeda 1.0 '{SF}.docx

./Fred Ranak:
Fred Ranak - Panica la Pentagon 1.0 '{Suspans}.docx

./Fredric Brown:
Fredric Brown - Arena 1.0 '{SF}.docx
Fredric Brown - Cioc 1.0 '{SF}.docx
Fredric Brown - Experiment 1.0 '{SF}.docx
Fredric Brown - Margarete 1.0 '{SF}.docx
Fredric Brown - Marionetele 1.0 '{SF}.docx
Fredric Brown - Nemurire 1.0 '{SF}.docx
Fredric Brown - Pamantenii purtatori de daruri 1.0 '{SF}.docx
Fredric Brown - Paradoxul pierdut 1.0 '{SF}.docx
Fredric Brown - Placed, planeta nebuna 0.99 '{SF}.docx
Fredric Brown - Raspunsul 1.0 '{SF}.docx
Fredric Brown - Sfarsit 1.0 '{SF}.docx

./Fredrik Backman:
Fredrik Backman - Bunica mi-a zis sa-ti spun ca-i pare rau 1.0 '{Tineret}.docx
Fredrik Backman - Noi contra voastra 1.0 '{Tineret}.docx
Fredrik Backman - Oameni anxiosi 1.0 '{Literatura}.docx
Fredrik Backman - Scandalul 1.0 '{Literatura}.docx
Fredrik Backman - Un barbat pe nume Ove 1.0 '{Literatura}.docx

./Fred Van Lente:
Fred Van Lente - Zece comici mititei 1.0 '{Diverse}.docx

./Fred Vargas:
Fred Vargas - Timpuri glaciare 1.0 '{Politista}.docx

./Fridtjof Nansen:
Fridtjof Nansen - Cu saniile spre Polul Nord 1.0 '{Calatorii}.docx

./Friedrich Durrenmatt:
Friedrich Durrenmatt - Fagaduiala 0.8 '{Literatura}.docx
Friedrich Durrenmatt - Justitie 0.99 '{Literatura}.docx

./Friedrich Gerstacker:
Friedrich Gerstacker - Omul padurilor 1.0 '{Western}.docx
Friedrich Gerstacker - Piratii de pe Mississippi 1.0 '{Western}.docx

./Friedrich Nietzsche:
Friedrich Nietzsche - Cazul Wagner 0.8 '{Filozofie}.docx
Friedrich Nietzsche - Dincolo de bine si de rau 1.0 '{Filozofie}.docx

./Friedrich Schiller:
Friedrich Schiller - Wallenstein V1 1.0 '{Teatru}.docx
Friedrich Schiller - Wallenstein V2 1.0 '{Teatru}.docx

./Fritjof Tito Colliander:
Fritjof Tito Colliander - Calea ascetilor 0.8 '{Spiritualitate}.docx
Fritjof Tito Colliander - Credinta si trairea ortodoxiei 0.9 '{Spiritualitate}.docx

./Fritz Leiber:
Fritz Leiber - Corabia umbrelor 1.0 '{SF}.docx
Fritz Leiber - Marele joc al timpului 1.0 '{CalatorieinTimp}.docx
Fritz Leiber - Padurea fermecata 0.9 '{SF}.docx

./Fritz Ridenour:
Fritz Ridenour - De fapt care este diferenta 0.9 '{Spiritualitate}.docx

./Fujiwara No Teika:
Fujiwara No Teika - O suta de poezii 0.9 '{Versuri}.docx

./Funeriu I.:
Funeriu I. - Eseuri lingvistice antitotalitare 0.7 '{Diverse}.docx

./Fustel de Coulanges:
Fustel de Coulanges - Cetatea antica V1 0.9 '{Spiritualitate}.docx

./Fust Milan:
Fust Milan - Povestea nevestei mele 1.0 '{Dragoste}.docx
```

